<?php

return [
    'endpoint' => env('COINAPI_ENDPOINT', 'https://v2-test.coin-api.com/'),
    'key'      => env('COINAPI_KEY'),
    'secret'   => env('COINAPI_SECRET')
];
