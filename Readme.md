Howhuge/coin-api
===
區塊鏈代收代付 SDK

對接詳情請聯絡: https://t.me/hi_btc
## Documentation
https://hugefan.notion.site/Coin-API-v2-db7587c4755c41078847eefca69d54d9

### Step.1 安裝 composer 套件
更改 `composer.json` 然後執行 `composer update`
```json
"require": {
    "howhuge/coin-api": "dev-master"
},
"repositories": [
    {
        "name" : "howhuge/coin-api",
        "type": "git",
        "url"  : "git@gitlab.com:howhuge/package/coin-api.git"
    }
]
```
### Step.2 publish config (Laravel 專案才需要)
```bash
php artisan vendor:publish --provider="Howhuge\CoinApi\ServiceProvider" --tag="coin-api"
```
並且設定`config/con-api.php` 對應的 key, secret

## Example
* Pure PHP
```php
<?php
    use Howhuge\CoinApi\CoinApi;

    $key = 'rzw72KS5DNjTdeAdTTrhpWwOtO5oMvzg';
    $secret = 'tdDkN3AxMLbcIM7vW04gVgbiDK6u60liKHdLHczH4nhTr8yZYfiBLVbOX8S1kcCB';
    $endpoint = 'http://xxxx.xxx/';

    $coinApi = new CoinApi($endpoint, $key, $secret);
    $coins = $coinApi->getCoin();

```
* Laravel
```php
<?php
    use Howhuge\CoinApi\Facades\CoinApi;
    // 更多 API 調用方式請至
    // `Howhuge\CoinApi\CoinApi.php` 查看調用

    // 創建用戶錢包
    $wallet = CoinApi::createWallet('huge');
    // {#9985
    //   +"code": 200
    //   +"data": array:1 [
    //     0 => {#9970
    //       +"identity": "huge"
    //       +"chain": "TRON"
    //       +"address": "TYdsAewBvkgpUkSJ5wMTsoHEuh4KWVh5js"
    //     }
    //   ]
    // }

    // 取得代幣資訊
    $coins = CoinApi::getCoin();
    // {#1518
    //   +"code": 200
    //   +"data": array:2 [
    //     0 => {#1521
    //       +"name": "TRX"
    //       +"title": "TRX"
    //       +"symbol": "TRX"
    //       +"chain": "TRON"
    //       +"fee": "1.00000000"
    //     }
    //     1 => {#1674
    //       +"name": "USDT_TRC20"
    //       +"title": "Tether"
    //       +"symbol": "USDT"
    //       +"chain": "TRON"
    //       +"fee": "1.00000000"
    //     }
    //   ]
    // }
```
