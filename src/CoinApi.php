<?php

namespace Howhuge\CoinApi;

use Howhuge\CoinApi\Singer\Signature;

class CoinApi extends Requestable
{
    private $version = 'v1';

    protected $http;

    protected $signature;

    const TYPE = [
        'WITHDRAW' => 0,
        'DEPOSITE' => 1,
    ];

    const STATUS = [
        'QUEUE' => 0,
        'TRANSACTION' => 1,
        'COMPLETE' => 2,
        'FAIL' => 3,
    ];

    public function __construct(string $endpoint, string $key, string $secret)
    {
        $this->setKey($key);
        $this->setEndpoint($endpoint);
        $this->signature = new Signature($secret);
    }

    /**
     *    取得狀態對應的代碼
     *
     * @param  string  $text  [description]
     * @return [type]       [description]
     */
    public function statusCode(string $key)
    {
        return self::STATUS[$key] ?? $key;
    }

    /**
     *    取得狀態對應的代碼
     *
     * @param  string  $text  [description]
     * @return [type]       [description]
     */
    public function typeCode(string $key)
    {
        return self::TYPE[$key] ?? $key;
    }

    /**
     *    驗簽
     *
     *    @param  [type] $sign   [description]
     * @param  array  $params  [description]
     * @return [type]         [description]
     */
    public function checkSign($sign, array $params)
    {
        return $this->signature->check($params, $sign);
    }

    /**
     *    取得代幣資訊
     *
     *    @return [type] [description]
     */
    public function getCoin()
    {
        $response = $this->request('GET', "api/{$this->version}/coin");

        return $this->parse($response);
    }

    /**
     *    取得錢包地址
     *
     *    @return [type] [description]
     */
    public function createWallet(string $identity, $chains = 'TRON')
    {
        $response = $this->request('POST', "api/{$this->version}/wallet", [
            'identity' => $identity,
            'chains' => $chains,
        ]);

        return $this->parse($response);
    }

    /**
     *    取得錢包地址
     *
     *    @return [type] [description]
     */
    public function getWallet(string $value, $chains = ['TRON'], $searchColumn = 'identity')
    {
        $response = $this->request('GET', "api/{$this->version}/wallet", [
            $searchColumn => $value,
            'chains' => $chains,
        ]);

        return $this->parse($response);
    }

    /**
     *    查詢錢包餘額
     *
     *    @return [type] [description]
     */
    public function getAsset(string $identity, $coins = null)
    {
        $response = $this->request('GET', "api/{$this->version}/asset", [
            'identity' => $identity,
            'coins' => $coins,
        ]);

        return $this->parse($response);
    }

    /**
     *    查詢用戶交易訂單
     *
     *    @return [type] [description]
     */
    public function getOrder(string $identity, array $queryScope = [])
    {
        $queryScope = array_merge(['identity' => $identity], $queryScope);

        $response = $this->request('GET', "api/{$this->version}/order", $queryScope);

        return $this->parse($response);
    }

    /**
     *    創建提現交易
     *
     *    @return [type] [description]
     */
    public function createOrder(string $coin, string $order_no, string $from, string $to, $qty, string $skipCheck = '0')
    {
        $response = $this->request('POST', "api/{$this->version}/order/withdraw", [
            'coin' => $coin,
            'order_no' => $order_no,
            'from' => $from,
            'to' => $to,
            'qty' => $qty,
            'skip_balacne_check' => $skipCheck,
        ]);

        return $this->parse($response);
    }

    /**
     *    收集用戶資產至商戶錢包 或是指定地址
     *    (若為指定地址須先申請否則一率轉至後台商戶錢包)
     *
     *    @return [type] [description]
     */
    public function collectUserAsset(string $coin, string $order_no, string $from, string $to, $qty)
    {
        $response = $this->request('POST', "api/{$this->version}/merchant/withdraw", [
            'identity' => $coin,
            'new_identity' => $new_identity,
        ]);

        return $this->parse($response);
    }

    /**
     *    更換用戶的 identity
     *
     *    @return [type] [description]
     */
    public function changeInfo(string $identity, string $newIdentity)
    {
        $response = $this->request('POST', "api/{$this->version}/wallet/change", [
            'identity' => $identity,
            'new_identity' => $newIdentity,
        ]);

        return $this->parse($response);
    }
}
