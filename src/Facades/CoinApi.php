<?php

namespace Howhuge\CoinApi\Facades;

use Illuminate\Support\Facades\Facade;

class CoinApi extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \Howhuge\CoinApi\CoinApi::class;
    }
}
