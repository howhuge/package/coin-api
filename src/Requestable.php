<?php

namespace Howhuge\CoinApi;

use Throwable;
use Exception;
use GuzzleHttp\Client;

abstract class Requestable
{
    private $endpoint;
    private $key;

    protected function parse($response, $key = null)
    {
        $response = json_decode($response->getBody());

        return $response;
    }

    public function setEndpoint(string $endpoint)
    {
        $this->endpoint = $this->formatDomain($endpoint);
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function header($parameter)
    {
        return [
            'X-Api-Key'  => $this->key,
            'X-Api-Sign' => $this->signature->encrypt($parameter),
        ];
    }

    public function request($method, $uri, $params = [])
    {
        $client = new Client();

        $endpoint = $this->endpoint . $uri;

        try {
            $method = strtoupper($method);

            $options = [
                'headers' => $this->header(array_filter($params))
            ];

            $options = $method === 'GET' ? array_merge($options, [
                'query' => $params
            ]) : array_merge($options, [
                'form_params' => $params
            ]);

            $response = $client->request($method, $endpoint, array_filter($options));

            return $response;
        } catch (Throwable $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function formatDomain(string $domain)
    {
        return substr($domain, -1) == '/'
            ? $domain
            : $domain . '/';
    }
}
