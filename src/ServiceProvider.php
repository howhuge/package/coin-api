<?php

namespace Howhuge\CoinApi;

use Howhuge\CoinApi\CoinApi;
use Illuminate\Support\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/coin-api.php' => config_path('coin-api.php'),
        ], 'coin-api');
    }

    public function register()
    {
        $this->app->singleton(CoinApi::class, function ($app) {
            $config = $app['config']['coin-api'];
            return new CoinApi($config['endpoint'], $config['key'], $config['secret']);
        });
    }
}
