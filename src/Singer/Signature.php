<?php

namespace Howhuge\CoinApi\Singer;

class Signature
{
    private $cipher = 'AES-256-ECB';
    private $privateKey;
    private $timestamp;

    public function __construct(string $privateKey)
    {
        $this->privateKey = $privateKey;
    }

    /**
     *    加密
     *    @param  array  $parameter [description]
     *    @return [type]            [description]
     */
    public function encrypt(array $parameter): string
    {
        ksort($parameter);

        $json = json_encode($parameter, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        $crypt = openssl_encrypt($json, $this->cipher, $this->privateKey, OPENSSL_RAW_DATA);

        $crypt = base64_encode($crypt);

        $crypt = hash('sha256', $crypt);

        return $crypt;
    }

    /**
     *    驗簽
     *    @param  array  $paramater [description]
     *    @param  string $sign      [description]
     *    @return [type]            [description]
     */
    public function check(array $paramater, string $sign)
    {
        return $this->encrypt($paramater) === $sign;
    }
}
