<?php

namespace Howhuge\CoinApi\Tests\Feature;

use Carbon\Carbon;
use Howhuge\CoinApi\CoinApi;
use Howhuge\CoinApi\Tests\TestCase;

class CoinApiTest extends TestCase
{
    private $sdk;

    public function setUp(): void
    {
        $key = 'jaSYRbi796GE2FmBxox7pZLZqL0RX0q4';
        $secret = '7eUuRAehUU5AESQdumbEJmMTy0ncSyxhf3wGvmHvR3BxiInR1aCB3WiFsZb4mgJZ';
        $endpoint = 'http://pay.dd/';

        $this->sdk = new CoinApi($endpoint, $key, $secret);
    }

    /**
     *    @test
     */
    public function 創建提現交易()
    {
        $response = $this->sdk->createOrder(
            'TRX',
            'O' . Carbon::now()->timestamp,
            'TZ6DrDPTFxSkRgfmSmW84VNc9bvHxhraFP',
            'TMFCjRriVsMs5crVGfd9aHKhTHKCmETz4g',
            '1.1'
        );

        $this->assertEquals(200, $response->code);
        $data = $response->data;
        $this->assertNotEmpty('sid', $data->sid);
        $this->assertNotEmpty('order_no', $data->order_no);
        $this->assertNotEmpty('status', $data->status);
        $this->assertNotEmpty('from', $data->from);
        $this->assertNotEmpty('to', $data->to);
        $this->assertNotEmpty('type', $data->type);
        $this->assertNotEmpty('qty', $data->qty);
        $this->assertNotEmpty('fee', $data->fee);
        $this->assertNotEmpty('qty_after_fee', $data->qty_after_fee);
        $this->assertNotEmpty('identity', $data->identity);
        $this->assertNotEmpty('coin', $data->coin);
        $this->assertNotEmpty('symbol', $data->symbol);
        $this->assertNotEmpty('chain', $data->chain);
        $this->assertNotEmpty('created_at', $data->created_at);
    }

    /**
     *    @test
     */
    public function 查詢用戶交易訂單()
    {
        $response = $this->sdk->getOrder('test1627738393');

        $this->assertEquals(200, $response->code);

        collect($response->data)->each(function ($response) {
            $response = (array)$response;
            $this->assertArrayHasKey('sid', $response);
            $this->assertArrayHasKey('txid', $response);
            $this->assertArrayHasKey('order_no', $response);
            $this->assertArrayHasKey('status', $response);
            $this->assertArrayHasKey('type', $response);
            $this->assertArrayHasKey('from', $response);
            $this->assertArrayHasKey('to', $response);
            $this->assertArrayHasKey('qty', $response);
            $this->assertArrayHasKey('fee', $response);
            $this->assertArrayHasKey('qty_after_fee', $response);
            $this->assertArrayHasKey('identity', $response);
            $this->assertArrayHasKey('coin', $response);
            $this->assertArrayHasKey('symbol', $response);
            $this->assertArrayHasKey('chain', $response);
            $this->assertArrayHasKey('created_at', $response);
        });
    }

    /**
     *    @test
     */
    public function 取得代幣資訊()
    {
        $response = $this->sdk->getCoin();
        $this->assertEquals(200, $response->code);

        collect($response->data)->each(function ($response) {
            $response = (array)$response;
            $this->assertArrayHasKey('name', $response);
            $this->assertArrayHasKey('title', $response);
            $this->assertArrayHasKey('symbol', $response);
            $this->assertArrayHasKey('chain', $response);
            $this->assertArrayHasKey('fee', $response);
        });
    }

    /**
     *    @test
     */
    public function 創建錢包資訊()
    {
        $userID = 'test' . Carbon::now()->timestamp;

        $response = $this->sdk->createWallet($userID, 'TRON');

        $this->assertEquals(200, $response->code);

        collect($response->data)->each(function ($response) {
            $response = (array)$response;
            $this->assertArrayHasKey('chain', $response);
            $this->assertArrayHasKey('address', $response);
        });
    }

    /**
     *    @test
     */
    public function 取得錢包資訊()
    {
        $response = $this->sdk->getWallet('test1627732868');
        $this->assertEquals(200, $response->code);

        collect($response->data)->each(function ($response) {
            $response = (array)$response;
            $this->assertArrayHasKey('chain', $response);
            $this->assertArrayHasKey('address', $response);
        });
    }

    /**
     *    @test
     */
    public function 取得錢包餘額()
    {
        $response = $this->sdk->getAsset('test1627732868');

        $this->assertEquals(200, $response->code);
        $this->assertEquals('test1627732868', $response->data->identity);

        collect($response->data->assets)->each(function ($response) {
            $response = (array)$response;
            $this->assertArrayHasKey('symbol', $response);
            $this->assertArrayHasKey('name', $response);
            $this->assertArrayHasKey('qty', $response);
        });
    }
}
