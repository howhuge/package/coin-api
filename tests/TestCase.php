<?php

namespace Howhuge\CoinApi\Tests;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function getApplicationTimezone($app)
    {
        return 'Asia/Taipei';
    }

    protected function getPackageProviders($app)
    {
        return [
            \Howhuge\CoinApi\ServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config  = require __DIR__ . '/../config/coin-api.php';

        $app['config']->set('coin-api', $config);
    }
}
